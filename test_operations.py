from a_plus_b import add_numbers, subtract_numbers, multiply_numbers, divide_numbers


def test_add_numbers():
    assert add_numbers(1, 2) == 3


def test_subtract_numbers():
    assert subtract_numbers(1, 2) == -1


def test_multiply_numbers():
    assert multiply_numbers(3, 4) == 12


def test_divide_numbers():
    assert divide_numbers(1, 2) == 0.5
