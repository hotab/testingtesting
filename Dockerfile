FROM python:3.12 AS production

ARG POETRY_VERSION=1.5.1

ENV POETRY_VERSION=${POETRY_VERSION} \
    PYTHONPATH=/app \
    PIP_NO_CACHE_DIR=1 \
    POETRY_NO_INTERACTION=1

RUN pip install --no-cache-dir poetry~=$POETRY_VERSION

WORKDIR /app
COPY pyproject.toml poetry.lock* ./

RUN poetry config virtualenvs.in-project true && \
    poetry install --no-dev # Installs to /app/.venv

ENV PATH="/app/.venv/bin:$PATH"

COPY *.py ./

CMD ["python", "a_plus_b.py", "--host", "0.0.0.0", "--port", "8000"]

FROM production AS testing

COPY reports reports

RUN poetry install

CMD ["pytest", ".", "--junitxml=reports/report.xml"]
