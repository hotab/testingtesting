import pytest

from a_plus_b import main


class Args:
    def __init__(self, a, b, o):
        self.a = a
        self.b = b
        self.o = o


def test_add():
    args = Args('1', '2', 'add')
    assert main(args) == '3'


def test_parse_fail():
    args = Args('1', 'hello', 'add')
    with pytest.raises(ValueError):
        main(args)


