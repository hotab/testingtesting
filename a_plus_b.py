import argparse


def add_numbers(a: int, b: int) -> int:
    return a + b


def subtract_numbers(a: int, b: int) -> int:
    return a - b


def multiply_numbers(a: int, b: int) -> int:
    return a * b


def divide_numbers(a: int, b: int) -> float:
    return a / b


def main(args):
    map_ops = {
        'add': add_numbers,
        'subtract': subtract_numbers,
        'multiply': multiply_numbers,
        'divide': divide_numbers
    }
    if args.o not in map_ops:
        return False
    return f'{map_ops[args.o](int(args.a), int(args.b))}'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', help='First number', required=True)
    parser.add_argument('-b', help='Second number', required=True)
    parser.add_argument('-o', help='Operation', required=True)
    args_parsed = parser.parse_args()
    result = main(args_parsed)
    if not result and isinstance(result, bool):
        print('Invalid operation')
    else:
        print(result)

